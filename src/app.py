import subprocess
from functools import wraps

from flask import Flask, render_template, make_response
from flask import request, redirect, url_for
from icecream import icecream

from jinja.filters import startswith

from models.Organization import Organization
from app_services.TelegramBot import TelegramBot
from utils.organization_storage import load_organizations

app = Flask(__name__)
app.jinja_env.filters['startswith'] = startswith

organizations = load_organizations()


def token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        oauth_token = request.cookies.get('oauth_token')
        if oauth_token and oauth_token in organizations:
            return f(oauth_token, *args, **kwargs)
        else:
            return redirect(url_for('sign_in'))

    return decorated_function


@app.route('/', methods=['GET'])
def root():
    return redirect(url_for('sign_in'))


@app.route('/sign_up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        data = request.form
        new_project = Organization(
            name=data['organization_name'],
            oauth_token=data['oauth_token'],
            org_id=data['org_id'],
            telegram_token=data['telegram_token']
        )
        new_bot = TelegramBot(data['telegram_token'], new_project)
        new_bot.start()
        organizations[data['oauth_token']] = new_project
        response = redirect(url_for('get_projects', oauth_token=data['oauth_token']))
        response.set_cookie('oauth_token', data['oauth_token'], httponly=True)
        return response
    return render_template('sign_up.html')


@app.route('/sign_in', methods=['GET'])
def sign_in():
    oauth_token = request.args.get('oauth_token')
    if oauth_token in organizations:
        response = redirect(url_for('get_projects'))
        response.set_cookie('oauth_token', oauth_token, httponly=True)
        return response
    else:
        return render_template('sign_in.html', error="Проект не найден")


@app.route('/projects', methods=['GET'])
@token_required
def get_projects(oauth_token):
    org = organizations[oauth_token]
    organization = {
        'name': org.name,
        'projects': org.get_projects()
    }
    return render_template('projects.html', organization=organization)


@app.route('/project/<int:project_id>', methods=['GET'])
@token_required
def get_project_info(oauth_token, project_id):
    org = organizations[oauth_token]
    project = org.project_by_id(project_id)
    if not project:
        return redirect(url_for('get_projects', token=oauth_token))

    project_chats, available_chats = org.get_chats_by_project_id(project_id)
    statistics = org.get_project_statistics(project_id)
    icecream.ic(statistics)
    project_data = {
        'name': org.name,
        'project': project,
        'workers': org.get_workers_by_project_id(project_id),
        'recommendations': org.get_recommendations_by_project_id(project_id),
        'project_chats': project_chats,
        'available_chats': available_chats,
        'statistics': statistics
    }
    return render_template('project.html', organization=project_data)


@app.route('/project/<project_id>/worker/<int:worker_id>', methods=['GET'])
@token_required
def get_project_worker_info(oauth_token, project_id, worker_id):
    org = organizations[oauth_token]
    project = org.project_by_id(project_id)
    if not project:
        return redirect(url_for('get_projects', token=oauth_token))

    worker = org.worker_by_id(worker_id)

    selected_telegram_username = ''
    if str(worker_id) in org.username_bindings:
        selected_telegram_username = org.username_bindings[str(worker_id)]
    communication_data = org.get_worker_communication_data_by_project_id(worker_id, project_id)
    tracker_data = org.get_worker_issue_performance_data_by_project_id(worker_id, project_id)
    recommendations = org.generate_recommendations(communication_data, tracker_data)
    project_chats, available_chats = org.get_chats_by_project_id(project_id)

    data = {
        'name': org.name,
        'project': project,
        'workers': org.get_workers_by_project_id(project_id),
        'selected_worker': worker,
        'usernames': org.telegram_users,
        'queues': org.projects,
        'issues': org.get_issues_by_project_id(project_id),
        'telegram_usernames': org.telegram_users,
        'selected_worker_telegram_username': selected_telegram_username,
        'communication_data': communication_data,
        "tracker_data": tracker_data,
        "settings": org.settings.settings,
        "worker_recommendations": recommendations,
        'recommendations': org.get_recommendations_by_project_id(project_id),
        'project_chats': project_chats,
        'available_chats': available_chats
    }
    return render_template('project.html', organization=data)


@app.route('/project/<project_id>/add_chat', methods=['POST'])
@token_required
def add_chat_to_project(oauth_token, project_id):
    org = organizations[oauth_token]
    chat_id = request.form.get('chat_id')
    icecream.ic(chat_id)
    if chat_id:
        org.add_chat_to_project(project_id, chat_id)
    return redirect(url_for('get_project_info', project_id=project_id, token=oauth_token))


@app.route('/project/<project_id>/remove_chat/<chat_id>', methods=['POST'])
@token_required
def remove_chat_from_project(oauth_token, project_id, chat_id):
    org = organizations[oauth_token]
    org.remove_chat_from_project(project_id, chat_id)
    return redirect(url_for('get_project_info', project_id=project_id, token=oauth_token))


@app.route('/workers', methods=['GET'])
@token_required
def get_workers(oauth_token):
    org = organizations[oauth_token]
    statistics = org.get_organization_statistics()
    icecream.ic(statistics)
    organization = {
        'name': org.name,
        'workers': org.workers,
        'recommendations': org.get_recommendations_by_worker(),
        'statistics': statistics
    }
    return render_template('worker.html', organization=organization)


@app.route('/workers/<int:worker_id>', methods=['GET'])
@token_required
def get_worker_info(oauth_token, worker_id):
    org = organizations[oauth_token]
    worker = next((w for w in org.workers if w['passportUid'] == worker_id), None)
    selected_telegram_username = ''
    if str(worker_id) in org.username_bindings:
        selected_telegram_username = org.username_bindings[str(worker_id)]
    communication_data = org.get_worker_communication_data(worker_id)
    tracker_data = org.get_worker_issue_performance_data(worker_id)
    recommendations = org.generate_recommendations(communication_data, tracker_data)
    rec = org.get_recommendations_by_worker()
    data = {
        'name': org.name,
        'workers': org.workers,
        'selected_worker': worker,
        'usernames': org.telegram_users,
        'queues': org.projects,
        'issues': org.issues,
        'telegram_usernames': org.telegram_users,
        'selected_worker_telegram_username': selected_telegram_username,
        'communication_data': communication_data,
        "tracker_data": tracker_data,
        "settings": org.settings.settings,
        "worker_recommendations": recommendations,
        'recommendations': rec
    }
    return render_template('worker.html', organization=data)


@app.route('/workers/<int:worker_id>/update_telegram_username', methods=['POST'])
@token_required
def update_telegram_username(oauth_token, worker_id):
    project = organizations[oauth_token]
    new_username = request.form.get('telegramUsername')
    project.bind_username(worker_id, new_username)
    return redirect(url_for('get_worker_info', worker_id=worker_id))


@app.route('/settings', methods=['GET'])
@token_required
def get_settings(oauth_token):
    org = organizations[oauth_token]
    organization = {
        'name': org.name,
        'settings': org.settings.settings,
        'default_settings': org.settings.default_settings
    }
    return render_template('settings.html', organization=organization)


@app.route('/settings', methods=['POST'])
@token_required
def update_settings(oauth_token):
    org = organizations[oauth_token]
    thresholds = {}
    for key in request.form.keys():
        thresholds[key] = float(request.form.get(key, org.settings.default_settings.get(key)))
    updated_settings = {
        "thresholds": thresholds
    }
    org.update_settings(updated_settings)

    return redirect(url_for('get_settings', oauth_token=oauth_token))


@app.route('/logout', methods=['POST'])
def logout():
    response = make_response(redirect(url_for('sign_in')))
    response.delete_cookie('oauth_token')
    return response


if __name__ == '__main__':
    app.run(debug=False)
