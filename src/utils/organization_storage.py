import json
import os

from app_services.TelegramBot import TelegramBot
from models.Organization import Organization


def load_organizations():
    organizations = {}
    if not os.path.exists('data'):
        return organizations
    for file_name in os.listdir('data'):
        if file_name.endswith('.json'):
            file_path = os.path.join('data', file_name)
            with open(file_path, 'r') as f:
                project_data = json.load(f)

                project = Organization(**project_data)
                organizations[project.oauth_token] = project
                new_bot = TelegramBot(project_data['telegram_token'], project)
                new_bot.start()
    return organizations
