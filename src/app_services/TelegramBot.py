import icecream
import telebot
import threading


class TelegramBot:
    def __init__(self, token, project):
        self.bot = telebot.TeleBot(token)
        self.project = project
        self.init_handlers()

    def init_handlers(self):
        @self.bot.message_handler(func=lambda message: True)
        def handle_messages(message):
            if message.json['chat']['type'] != 'group':
                return
            self.project.add_telegram_message(message.json)
            print(f"Сообщение от {message.from_user.username}: {message.text}")

    def start(self):
        thread = threading.Thread(target=self.bot.polling, kwargs={'none_stop': True})
        thread.start()
