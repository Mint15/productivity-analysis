import subprocess
import time
from datetime import datetime

from dostoevsky.tokenization import RegexTokenizer
from dostoevsky.models import FastTextSocialNetworkModel
from icecream import icecream


class CommunicationStyleAnalyzer:
    def __init__(self):
        self.tokenizer = RegexTokenizer()
        self.model = FastTextSocialNetworkModel(tokenizer=self.tokenizer)

    def _analyze_messages(self, messages):
        results = self.model.predict(messages, k=2)
        return results

    @staticmethod
    def _summarize_sentiments(analysis_results):
        sentiment_summary = {}
        messages_count = len(analysis_results)
        for result in analysis_results:
            if result.get('skip', 0) > 0.5 or result.get('speech', 0) > 0.5:
                messages_count = messages_count - 1
                continue

            for sentiment, value in result.items():
                if sentiment in ['skip', 'speech']:
                    continue
                capped_value = min(value, 1)
                if sentiment in sentiment_summary:
                    sentiment_summary[sentiment] += capped_value
                else:
                    sentiment_summary[sentiment] = capped_value
        if messages_count <= 0:
            return {}
        for sentiment in sentiment_summary:
            sentiment_summary[sentiment] /= messages_count

        return sentiment_summary

    @staticmethod
    def _normalize_sentiments(sentiment_dict):
        total = sum(sentiment_dict.values())
        normalized_sentiments = {key: value / total for key, value in sentiment_dict.items()}
        return normalized_sentiments

    def analyze(self, messages):
        results = self._analyze_messages(messages)
        results = self._summarize_sentiments(results)
        results = self._normalize_sentiments(results)
        return results

    @staticmethod
    def compute_average_answer_time(chats, username):
        total_response_time = 0
        response_counts = 0

        for chat_id, messages in chats.items():
            mentioned = False
            last_mention_time = None

            for message in messages:
                message_details = next(iter(message.values()))
                message_text = message_details['text']
                message_from = message_details['from']['username'] if 'username' in message_details['from'] else None
                message_date = datetime.fromtimestamp(message_details['date'])

                if '@' + username in message_text and message_from != username:
                    mentioned = True
                    last_mention_time = message_date
                elif message_from == username and mentioned:
                    if last_mention_time:
                        response_time = (message_date - last_mention_time).total_seconds()
                        total_response_time += response_time
                        response_counts += 1
                        mentioned = False

        if response_counts == 0:
            return None
        return total_response_time / response_counts
