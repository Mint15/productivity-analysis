import re
from collections import defaultdict

from icecream import ic
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.util import ngrams
from pymorphy2 import MorphAnalyzer


def _additional_stopwords():
    return {'баг', 'фактический', 'результат', 'ожидать', 'должный'}


def _paired_stopwords():
    return {'заданный', 'соответствовать', 'соответствующий', 'слишком', 'неправильно', 'правильно', 'нормально'}


class BugReportAnalyzer:
    def __init__(self, threshold_count, result_limit, ngram_size=5):
        self.threshold_count = int(threshold_count)
        self.result_limit = int(result_limit)
        self.ngram_size = ngram_size
        self.stop_words = set(stopwords.words('russian'))
        self.morph = MorphAnalyzer()
        self.paired_stopwords = _paired_stopwords()
        self.normal_form_cache = {}

    def analyze_bugs(self, issues):
        ngram_details = defaultdict(lambda: {"count": 0, "comments": [], "display": ""})

        for issue in issues:
            seen_comments = set()
            for comment_text in issue.get('comments', []):
                if self._is_relevant_comment(comment_text) and comment_text not in seen_comments:
                    tokenized, original_tokens = self._tokenize_and_clean(comment_text)
                    seen_ngrams = set()

                    for n in range(1, self.ngram_size + 1):
                        for ngram in ngrams(tokenized, n):
                            ngram_str = ' '.join(ngram)
                            if ngram_str not in seen_ngrams:
                                seen_ngrams.add(ngram_str)
                                if ngram_str not in ngram_details:
                                    ngram_details[ngram_str]["display"] = ' '.join(
                                        next((t for t in zip(*[original_tokens[i:] for i in range(len(ngram))])
                                              if all(self.get_normal_form(token.lower()) == norm
                                                     for token, norm in zip(t, ngram))), ngram_str.split()[0])
                                    )
                                ngram_details[ngram_str]["count"] += 1
                                ngram_details[ngram_str]["comments"].append(
                                    {"text": comment_text, "issue_url": issue['url'], "issue_name": issue["summary"]}
                                )

                    seen_comments.add(comment_text)

        # Convert comments to tuple for hashing
        for details in ngram_details.values():
            details["comments"] = [tuple(comment.items()) for comment in details["comments"]]

        # Sort ngram_details by count and length
        sorted_ngram_details = sorted(
            ngram_details.items(),
            key=lambda item: (-item[1]["count"], -len(item[0].split()))
        )

        filtered_ngram_details = {}
        seen_comment_sets = set()

        for ngram, details in sorted_ngram_details:
            comment_set = frozenset(details["comments"])
            if comment_set not in seen_comment_sets:
                filtered_ngram_details[ngram] = details
                seen_comment_sets.add(comment_set)

        for details in filtered_ngram_details.values():
            details["comments"] = [dict(comment) for comment in details["comments"]]

        frequent_words_details = [
            {"word": word, "count": details["count"], "comments": details["comments"], "display": details["display"]}
            for word, details in filtered_ngram_details.items()
            if details["count"] >= self.threshold_count
        ]
        frequent_words_details.sort(key=lambda x: x["count"], reverse=True)
        frequent_words_details = frequent_words_details[:self.result_limit]

        avg_bugs = sum(len(task['comments']) for task in issues if task['comments']) / len(issues) if issues else 0
        ic({details["display"]: details["count"] for word, details in filtered_ngram_details.items()})

        return {
            "average_bugs": avg_bugs,
            "frequent_words_details": frequent_words_details
        }

    def get_normal_form(self, word):
        if word not in self.normal_form_cache:
            self.normal_form_cache[word] = self.morph.parse(word)[0].normal_form
        return self.normal_form_cache[word]

    @staticmethod
    def _is_relevant_comment(comment):
        contains_bug = re.search(r'\b(Баг|Bug)\b', comment, re.IGNORECASE)
        not_contains_fixed = not re.search(
            r'\b(Исправлено|Fixed|Поправлено|Готово|Все\sбаги\sисправлены|Все\sпоправили|Ошибок\sбольше\sнет)\b',
            comment,
            re.IGNORECASE
        )
        return contains_bug and not_contains_fixed

    def _tokenize_and_clean(self, text):
        tokens = word_tokenize(text)
        cleaned_tokens = []
        original_tokens = []

        for token in tokens:
            normal_form = self.get_normal_form(token.lower())
            if token.lower() not in self.stop_words and normal_form not in _additional_stopwords() and token.isalpha():
                cleaned_tokens.append(normal_form)
                original_tokens.append(token)

        final_tokens = []
        final_original_tokens = []
        for i, token in enumerate(cleaned_tokens):
            if token not in self.paired_stopwords:
                final_tokens.append(token)
                final_original_tokens.append(original_tokens[i])
            else:
                if i > 0 and cleaned_tokens[i - 1] not in self.paired_stopwords:
                    final_tokens.append(token)
                    final_original_tokens.append(original_tokens[i])
                elif i < len(cleaned_tokens) - 1 and cleaned_tokens[i + 1] not in self.paired_stopwords:
                    final_tokens.append(token)
                    final_original_tokens.append(original_tokens[i])

        return final_tokens, final_original_tokens
