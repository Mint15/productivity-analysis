class RecommendationService:
    def __init__(self, settings):
        self.settings = settings

    def generate_recommendations(self, analytics_data):
        recommendations = set()

        thresholds = self.settings.get_settings()['thresholds']

        if analytics_data.get('overdue_task_ratio') is not None:
            if analytics_data['overdue_task_ratio'] > thresholds['overdue_task_ratio_critical']:
                recommendations.add('critical_overdue_tasks')
            elif analytics_data['overdue_task_ratio'] > thresholds['overdue_task_ratio_warning']:
                recommendations.add('warning_overdue_tasks')

        if analytics_data.get('overdue_time') is not None:
            if analytics_data['overdue_time'] > thresholds['overdue_time_critical']:
                recommendations.add('critical_overdue_tasks')
            elif analytics_data['overdue_time'] > thresholds['overdue_time_warning']:
                recommendations.add('warning_overdue_tasks')

        if analytics_data.get('response_time') is not None:
            if analytics_data['response_time'] > thresholds['response_time_critical']:
                recommendations.add('critical_response_time')
            elif analytics_data['response_time'] > thresholds['response_time_warning']:
                recommendations.add('warning_response_time')

        if analytics_data.get('negative_feedback_ratio') is not None:
            if analytics_data['negative_feedback_ratio'] > thresholds['negative_feedback_ratio_critical']:
                recommendations.add('critical_negative_feedback')
            elif analytics_data['negative_feedback_ratio'] > thresholds['negative_feedback_ratio_warning']:
                recommendations.add('warning_negative_feedback')

        if analytics_data.get('average_bugs_per_task') is not None:
            if analytics_data['average_bugs_per_task'] > thresholds['average_bugs_per_task_critical']:
                recommendations.add('critical_bugs_per_task')
            elif analytics_data['average_bugs_per_task'] > thresholds['average_bugs_per_task_warning']:
                recommendations.add('warning_bugs_per_task')

        criticals = {rec for rec in recommendations if 'critical' in rec}
        recommendations = [rec for rec in recommendations if
                           not ('warning' in rec and rec.replace('warning', 'critical') in criticals)]

        return recommendations
