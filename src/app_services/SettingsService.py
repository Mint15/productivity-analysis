import json


class SettingsService:
    def __init__(self, data=None):
        self.default_settings = {
            'thresholds': {
                'overdue_task_ratio_warning': 0.15,
                'overdue_task_ratio_critical': 0.25,
                'overdue_time_warning': 4.0,
                'overdue_time_critical': 8.0,
                'response_time_warning': 3600,
                'response_time_critical': 21600,
                'negative_feedback_ratio_warning': 0.15,
                'negative_feedback_ratio_critical': 0.3,
                'min_occurrences_for_word_count_warning': 5,
                'min_occurrences_for_word_count_critical': 10,
                'max_frequent_words_displayed': 10,
                'average_bugs_per_task_warning': 4,
                'average_bugs_per_task_critical': 8
            }
        }
        if data:
            self.settings = data
        else:
            self.settings = self.default_settings

    def update_settings(self, new_settings):
        def update_recursive(d, u):
            for k, v in u.items():
                if isinstance(v, dict):
                    d[k] = update_recursive(d.get(k, {}), v)
                else:
                    d[k] = v
            return d

        self.settings = update_recursive(self.settings, new_settings)

    def get_settings(self):
        return self.settings.copy()

    def to_dict(self):
        return self.settings.copy()
