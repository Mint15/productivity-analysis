import json

import icecream
import requests


class YandexTrackerService:

    def __init__(self, org_id, token, last_update):
        self.base_url = "https://api.tracker.yandex.net/v2/"
        self.base_user_link_url = "https://tracker.yandex.ru/"
        self.headers = {
            "Authorization": f"OAuth {token}",
            "X-Cloud-Org-ID": org_id,
            "Content-Type": "application/json"
        }
        self.last_update = last_update

    def _parse_issue(self, issue):
        return {
            "id": issue.get("id"),
            "key": issue.get("key"),
            "summary": issue.get("summary"),
            "description": issue.get("description"),
            "created_by_id": issue.get("createdBy", {}).get("passportUid"),
            "originalEstimation": issue.get("originalEstimation"),
            "changelog": self._get_issue_changelog(issue.get("key")),
            "comments": self._get_issue_comments(issue.get("key")),
            "url": self.base_user_link_url + issue.get("key"),
            "project_id": issue.get("project", {}).get("id")
        }

    def _get_issue_comments(self, issue_id):
        url = f"{self.base_url}issues/{issue_id}/comments"
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            comments = response.json()
            formatted_comments = [
                comment.get("text") for comment in comments
            ]
            return formatted_comments
        else:
            print(f"Ошибка при получении комментариев к задаче: {response.status_code}")
            return []

    def get_all_users(self):
        excluded_emails = [
            "yndx-wiki-cnt-robot@yandex.ru",
            "yndx-tracker-cnt-robot@yandex.ru",
            "yndx-forms-cnt-robot@yandex.ru"
        ]
        users = []
        url = f"{self.base_url}users"
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            all_users = response.json()
            users = [
                {
                    "trackerUid": user.get("trackerUid"),
                    "passportUid": user.get("passportUid"),
                    "display": user.get("display"),
                    "email": user.get("email")
                }
                for user in all_users if user.get('email') not in excluded_emails
            ]
        else:
            print(f"Ошибка: {response.status_code}")
        return users

    def get_all_projects(self):
        projects = []
        url = f"{self.base_url}projects"
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            all_projects = response.json()
            projects = [
                {
                    "id": project.get("id"),
                    "name": project.get("name"),
                    "lead_id": project.get("lead", {}).get("passportUid"),
                    "status": project.get("status"),
                    "description": project.get("description")
                }
                for project in all_projects
            ]
        else:
            print(f"Ошибка: {response.status_code}")
        return projects

    def get_all_issues(self, page=1, per_page=10):
        issues = []
        url = f"{self.base_url}issues/_search?page={page}&perPage={per_page}"
        params = {
            "filter": {
                "updated": {
                    "from": self.last_update.isoformat()
                }
            }
        }
        response = requests.post(url, data=json.dumps(params), headers=self.headers)
        if response.status_code == 200:
            all_issues = response.json()
            issues = [
                self._parse_issue(issue) for issue in all_issues
            ]
        elif response.status_code == 429:
            raise Exception("429 Too Many Requests")
        else:
            print(f"Ошибка: {response.status_code}, {response.text}")
        return issues

    def _get_issue_changelog(self, issue_id):
        required_fields = ["spent", "status"]
        url = f"{self.base_url}issues/{issue_id}/changelog"
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            change_history = response.json()
            filtered_history = []
            for entry in change_history:
                fields = []
                if 'fields' not in entry:
                    continue
                for field in entry['fields']:
                    if field['field']['id'] in required_fields:
                        if field['field']['id'] == "status":
                            from_value = field.get('from', {}).get('key') if field.get('from') else None
                            to_value = field.get('to', {}).get('key') if field.get('to') else None
                        else:
                            from_value = field.get('from')
                            to_value = field.get('to')

                        filtered_field = {
                            'field_id': field['field']['id'],
                            'from': from_value,
                            'to': to_value
                        }
                        fields.append(filtered_field)
                if fields:
                    filtered_entry = {
                        'fields': fields,
                        'updated_by': entry['updatedBy'].get('passportUid'),
                        'updated_at': entry['updatedAt']
                    }
                    filtered_history.append(filtered_entry)
            return filtered_history
        else:
            print(f"Ошибка при получении истории изменений задачи: {response.status_code}")
            return None

    def get_total_issues_count(self):
        url = f"{self.base_url}issues/_count"
        params = {
            "filter": {
                "updated": {
                    "from": self.last_update.isoformat()
                }
            }
        }
        response = requests.post(url, data=json.dumps(params), headers=self.headers)
        if response.status_code == 200:
            return response.json()
        else:
            print(f"Ошибка при получении количества задач: {response.status_code}, {response.text}")
            return 0
