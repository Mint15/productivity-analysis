import re

import icecream
from dateutil.parser import parse


class IssuePerformanceAnalyzer:

    @staticmethod
    def _parse_duration(duration):
        if duration is None:
            return 0
        hours = re.findall(r'\d+H', duration)
        hours = int(hours[0][:-1]) if hours else 0
        return hours

    def analyze_worker_performance(self, issues, passportUid):
        overdue_tasks = []
        total_tasks = 0
        total_estimated = 0
        total_spent = 0

        for issue in issues:
            if issue['originalEstimation'] is not None:
                estimation = self._parse_duration(issue['originalEstimation'])
                spent_time = 0
                in_progress = False

                for change in issue['changelog']:
                    for field in change['fields']:
                        if field['field_id'] == 'status':
                            if field['to'] == 'inProgress':
                                in_progress = True
                            elif in_progress and field['from'] == 'inProgress':
                                in_progress = False
                        elif field['field_id'] == 'spent' and change['updated_by'] == passportUid and in_progress:
                            # icecream.ic(self._parse_duration(field['to']), self._parse_duration(field['from']))
                            spent_time += self._parse_duration(field['to']) - self._parse_duration(field['from'])
                if spent_time > 0:
                    total_tasks += 1
                    total_spent += spent_time
                    total_estimated += estimation
                if spent_time > estimation:
                    overdue_tasks.append({
                        "summary": issue['summary'],
                        "url": issue['url'],
                        "overdue_value": spent_time - estimation
                    })

        overdue_ratio = len(overdue_tasks) / total_tasks if total_tasks > 0 else 0
        average_difference = (total_spent - total_estimated) / total_tasks if total_tasks > 0 else 0

        return {
            "total_issues": total_tasks,
            "overdue_ratio": overdue_ratio,
            "overdue_tasks": overdue_tasks,
            "average_difference": average_difference
        }
