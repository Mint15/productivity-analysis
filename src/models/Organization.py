import concurrent.futures
import json
import os
import random
from datetime import datetime
import time

from app_services.BugReportAnalyzer import BugReportAnalyzer
from app_services.CommunicationAnalyzer import CommunicationStyleAnalyzer
from app_services.IssuePerformanceAnalyzer import IssuePerformanceAnalyzer
from app_services.RecommendationService import RecommendationService
from app_services.SettingsService import SettingsService
from app_services.YandexTrackerService import YandexTrackerService


class Organization:
    def __init__(self, name, oauth_token, org_id, telegram_token, telegram_chats=None, telegram_messages=None,
                 telegram_users=None, username_bindings=None, workers=None, projects=None, issues=None,
                 settings_data=None, last_update=None, project_chats=None):
        self.name = name
        self.oauth_token = oauth_token
        self.org_id = org_id
        self.telegram_token = telegram_token
        self.telegram_chats = telegram_chats or {}
        self.telegram_messages = telegram_messages or {}
        self.telegram_users = telegram_users or []
        self.username_bindings = username_bindings or {}
        self.last_update = self._parse_last_update(last_update)
        self.tracker_service = YandexTrackerService(org_id=org_id, token=oauth_token, last_update=self.last_update)
        self.issues = issues or []
        self.workers = workers or self.tracker_service.get_all_users()
        self.projects = projects or self.tracker_service.get_all_projects()
        self.settings = SettingsService(settings_data)
        self.recommendation_service = RecommendationService(self.settings)
        self.project_chats = project_chats or {}
        self._start_update_issues_thread()
        self.save()

    def project_by_id(self, project_id):
        return next((pr for pr in self.projects if pr["id"] == str(project_id)), None)

    def worker_by_id(self, id):
        return next((w for w in self.workers if w["passportUid"] == id), None)

    def get_chats_by_project_id(self, project_id):
        project_chats = {key: self.telegram_chats[key] for key in self.project_chats.get(str(project_id), [])}
        available_chats = {key: value for key, value in self.telegram_chats.items() if key not in project_chats}
        return project_chats, available_chats

    @staticmethod
    def _parse_last_update(last_update):
        return datetime.fromisoformat(last_update) if isinstance(last_update, str) else (last_update or datetime.min)

    def bind_username(self, passport_uid, username):
        self.username_bindings[str(passport_uid)] = username
        self.save()

    def get_projects(self):
        def summarize_recommendations(recommendations_by_worker):
            summary = {
                'overdue_tasks': {'warning': 0, 'critical': 0, 'label': 'Просроченные задачи'},
                'response_time': {'warning': 0, 'critical': 0, 'label': 'Время ответа'},
                'negative_feedback': {'warning': 0, 'critical': 0, 'label': 'Негатив в общении'},
                'bugs_per_task': {'warning': 0, 'critical': 0, 'label': 'Баги'}
            }
            for worker_recommendations in recommendations_by_worker.values():
                for recommendation in worker_recommendations:
                    if recommendation.startswith('warning_'):
                        summary[recommendation.replace('warning_', '')]['warning'] += 1
                    elif recommendation.startswith('critical_'):
                        summary[recommendation.replace('critical_', '')]['critical'] += 1
            return summary

        updated_projects = []
        for project in self.projects:
            pr = project.copy()
            pr_id = pr["id"]
            pr["issues_count"] = len(self.get_issues_by_project_id(pr_id))
            pr["workers_count"] = len(self._get_workers_ids_by_project_id(pr_id))
            pr["chats_count"] = len(self.project_chats.get(str(pr_id), []))
            recommendations_by_worker = self.get_recommendations_by_project_id(pr_id)
            pr["recommendation_summary"] = summarize_recommendations(recommendations_by_worker)
            pr["has_recommendations"] = any(
                cat['warning'] + cat['critical'] > 0 for cat in pr["recommendation_summary"].values()
            )
            updated_projects.append(pr)
        return updated_projects

    def _start_update_issues_thread(self):
        concurrent.futures.ThreadPoolExecutor(max_workers=4).submit(self._update_issues)

    def _update_issues(self):
        total_issues = self.tracker_service.get_total_issues_count()
        total_pages = (total_issues // 10) + 1
        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            futures = [executor.submit(self._fetch_issues_page, page) for page in range(1, total_pages + 1)]
            for future in concurrent.futures.as_completed(futures):
                try:
                    self._process_issues(future.result())
                except Exception as e:
                    print(f"Ошибка при обновлении задач: {e}")
        self.last_update = datetime.now()
        self.save()

    def _fetch_issues_page(self, page):
        while True:
            try:
                time.sleep(random.random() * 2)
                print("Processing issues page ", page)
                return self.tracker_service.get_all_issues(page)
            except Exception as e:
                if '429' in str(e):
                    print(f"Received 429 error on page {page}. Retrying after delay...")
                    time.sleep(1)
                else:
                    raise e

    def _process_issues(self, issues):
        issue_dict = {issue['key']: issue for issue in self.issues}
        for updated_issue in issues:
            issue_key = updated_issue['key']
            if issue_key in issue_dict:
                issue_dict[issue_key] = updated_issue
            else:
                self.issues.append(updated_issue)
        print("Processed issues")
        self.save()

    def add_telegram_message(self, message):
        chat_id = str(message['chat']['id'])
        if chat_id not in self.telegram_chats:
            self.telegram_chats[chat_id] = message['chat']['title']
            self.telegram_messages[chat_id] = []
        self.telegram_messages[chat_id].append({message['message_id']: message})
        if message['from']['username'] not in self.telegram_users:
            self.telegram_users.append(message['from']['username'])
        self.save()

    def to_dict(self):
        return {
            'name': self.name,
            'oauth_token': self.oauth_token,
            'org_id': self.org_id,
            'telegram_token': self.telegram_token,
            'telegram_chats': self.telegram_chats,
            'telegram_messages': self.telegram_messages,
            'telegram_users': self.telegram_users,
            'username_bindings': self.username_bindings,
            'workers': self.workers,
            'projects': self.projects,
            'issues': self.issues,
            'settings_data': self.settings.to_dict(),
            'last_update': self.last_update.isoformat(),
            'project_chats': self.project_chats
        }

    def save(self):
        os.makedirs('data', exist_ok=True)
        file_path = os.path.join('data', f"{self.org_id}.json")
        with open(file_path, 'w', encoding='utf-8') as f:
            json.dump(self.to_dict(), f, ensure_ascii=False, indent=4)

    def get_worker_issues(self, worker_id):
        results, added_issues = [], set()
        for issue in self.issues:
            in_progress = any(
                field['field_id'] == 'status' and field['to'] == 'inProgress' for change in issue['changelog'] for field
                in change['fields'])
            if any(field['field_id'] == 'spent' and change['updated_by'] == worker_id and in_progress for change in
                   issue['changelog'] for field in change['fields']):
                if issue['key'] not in added_issues:
                    results.append(issue)
                    added_issues.add(issue['key'])
        return results

    def get_worker_messages(self, username):
        return [message for chat_messages in self.telegram_messages.values() for message_dict in chat_messages
                for message in message_dict.values() if message['from']['username'] == username]

    def get_worker_communication_data(self, worker_id):
        username = self.username_bindings.get(str(worker_id))
        if not username:
            return {}
        analyzer = CommunicationStyleAnalyzer()
        messages = self.get_worker_messages(username)
        message_texts = [message['text'] for message in messages if 'text' in message]
        return {
            'analytics': analyzer.analyze(message_texts),
            'average_answer_time': analyzer.compute_average_answer_time(self.telegram_messages, username),
            'messages_count': len(messages)
        }

    def get_worker_issue_performance_data(self, worker_id):
        analyzer = IssuePerformanceAnalyzer()
        thresholds = self.settings.settings.get("thresholds")
        bug_analyzer = BugReportAnalyzer(thresholds.get("min_occurrences_for_word_count_warning"),
                                         thresholds.get("max_frequent_words_displayed"))
        return {
            "performance": analyzer.analyze_worker_performance(self.issues, worker_id),
            "bug_report": bug_analyzer.analyze_bugs(self.get_worker_issues(worker_id))
        }

    def update_settings(self, settings_data):
        self.settings.update_settings(settings_data)
        self.save()

    def generate_recommendations(self, communication_data, performance_data):
        analytics_data = {
            'overdue_task_ratio': performance_data.get('performance', {}).get('overdue_ratio', 0),
            'overdue_time': performance_data.get('performance', {}).get('average_difference', 0),
            'response_time': communication_data.get('average_answer_time', 0),
            'negative_feedback_ratio': communication_data.get('analytics', {}).get('negative', 0),
            'average_bugs_per_task': performance_data.get('bug_report', {}).get('average_bugs', 0)
        }
        return self.recommendation_service.generate_recommendations(analytics_data=analytics_data)

    def get_issues_by_project_id(self, project_id):
        return [issue for issue in self.issues if issue['project_id'] == str(project_id)]

    def get_workers_by_project_id(self, project_id):
        return [self.worker_by_id(w_id) for w_id in self._get_workers_ids_by_project_id(project_id)]

    def _get_workers_ids_by_project_id(self, project_id):
        project_issues = self.get_issues_by_project_id(project_id)
        worker_ids = {change['updated_by'] for issue in project_issues for change in issue['changelog']
                      if
                      any(field['field_id'] == 'status' and field['to'] == 'inProgress' for field in change['fields'])}
        return list(worker_ids)

    def get_worker_issues_by_project_id(self, worker_id, project_id):
        return [issue for issue in self.get_worker_issues(worker_id) if issue['project_id'] == project_id]

    def get_recommendations_by_worker(self):
        return {str(worker['passportUid']): self.generate_recommendations(
            self.get_worker_communication_data(worker['passportUid']),
            self.get_worker_issue_performance_data(worker['passportUid'])
        ) for worker in self.workers}

    def get_recommendations_by_project_id(self, project_id):
        worker_ids = self._get_workers_ids_by_project_id(project_id)
        return {str(worker_id): self.generate_recommendations(
            self.get_worker_communication_data_by_project_id(worker_id, str(project_id)),
            self.get_worker_issue_performance_data_by_project_id(worker_id, str(project_id))
        ) for worker_id in worker_ids}

    def get_worker_communication_data_by_project_id(self, worker_id, project_id):
        username = self.username_bindings.get(str(worker_id))
        if not username:
            return {}
        analyzer = CommunicationStyleAnalyzer()
        messages = self.get_worker_messages_by_project_id(username, project_id)
        message_texts = [message['text'] for message in messages if 'text' in message]
        return {
            'analytics': analyzer.analyze(message_texts),
            'average_answer_time': analyzer.compute_average_answer_time(self.telegram_messages, username)
        }

    def get_worker_messages_by_project_id(self, username, project_id):
        chat_ids = self.project_chats.get(project_id, [])
        return [message for chat_id in chat_ids for message_dict in self.telegram_messages.get(chat_id, [])
                for message in message_dict.values() if message['from']['username'] == username]

    def get_worker_issue_performance_data_by_project_id(self, worker_id, project_id):
        analyzer = IssuePerformanceAnalyzer()
        thresholds = self.settings.settings.get("thresholds")
        bug_analyzer = BugReportAnalyzer(thresholds.get("min_occurrences_for_word_count_warning"),
                                         thresholds.get("max_frequent_words_displayed"))
        project_issues = self.get_worker_issues_by_project_id(worker_id, project_id)
        return {
            "performance": analyzer.analyze_worker_performance(project_issues, worker_id),
            "bug_report": bug_analyzer.analyze_bugs(project_issues)
        }

    def add_chat_to_project(self, project_id, chat_id):
        self.project_chats.setdefault(project_id, []).append(chat_id)
        self.save()

    def remove_chat_from_project(self, project_id, chat_id):
        if project_id in self.project_chats and chat_id in self.project_chats[project_id]:
            self.project_chats[project_id].remove(chat_id)
            if not self.project_chats[project_id]:
                del self.project_chats[project_id]
            self.save()

    def _compute_statistics(self, workers, project_id=None):
        valid_workers = []
        for worker in workers:
            worker_id = worker['passportUid']
            if project_id:
                communication_data = self.get_worker_communication_data_by_project_id(worker_id, project_id)
                performance_data = self.get_worker_issue_performance_data_by_project_id(worker_id, project_id)
            else:
                communication_data = self.get_worker_communication_data(worker_id)
                performance_data = self.get_worker_issue_performance_data(worker_id)

            if communication_data or performance_data:
                valid_workers.append((worker_id, communication_data, performance_data))

        total_workers = len(valid_workers)
        if total_workers == 0:
            return {}

        total_communication_data = {'average_answer_time': 0, 'negative_feedback_ratio': 0}
        total_performance_data = {'overdue_ratio': 0, 'average_difference': 0, 'average_bugs': 0}
        warning_counts = {'overdue_tasks': 0, 'response_time': 0, 'negative_feedback': 0, 'bugs_per_task': 0}
        critical_counts = {'overdue_tasks': 0, 'response_time': 0, 'negative_feedback': 0, 'bugs_per_task': 0}
        good_counts = {'overdue_tasks': 0, 'response_time': 0, 'negative_feedback': 0, 'bugs_per_task': 0}

        for worker_id, communication_data, performance_data in valid_workers:
            recommendations = self.generate_recommendations(communication_data, performance_data)

            for key in total_communication_data:
                total_communication_data[key] += communication_data.get(key, 0) or 0
            for key in total_performance_data:
                total_performance_data[key] += performance_data.get('performance', {}).get(key, 0) or 0

            has_warning_or_critical = {'overdue_tasks': False, 'response_time': False, 'negative_feedback': False,
                                       'bugs_per_task': False}

            for rec in recommendations:
                if rec.startswith('warning_'):
                    key = rec.replace('warning_', '')
                    warning_counts[key] += 1
                    has_warning_or_critical[key] = True
                elif rec.startswith('critical_'):
                    key = rec.replace('critical_', '')
                    critical_counts[key] += 1
                    has_warning_or_critical[key] = True

            for key in has_warning_or_critical:
                if not has_warning_or_critical[key]:
                    good_counts[key] += 1

        average_communication_data = {key: value / total_workers for key, value in total_communication_data.items()}
        average_performance_data = {key: value / total_workers for key, value in total_performance_data.items()}
        detailed_counts = {
            'overdue_tasks': {'warning': warning_counts['overdue_tasks'], 'critical': critical_counts['overdue_tasks'],
                              'good': good_counts['overdue_tasks']},
            'response_time': {'warning': warning_counts['response_time'], 'critical': critical_counts['response_time'],
                              'good': good_counts['response_time']},
            'negative_feedback': {'warning': warning_counts['negative_feedback'],
                                  'critical': critical_counts['negative_feedback'],
                                  'good': good_counts['negative_feedback']},
            'bugs_per_task': {'warning': warning_counts['bugs_per_task'], 'critical': critical_counts['bugs_per_task'],
                              'good': good_counts['bugs_per_task']}
        }

        return {
            'average_communication_data': average_communication_data,
            'average_performance_data': average_performance_data,
            'detailed_counts': detailed_counts
        }

    def get_organization_statistics(self):
        return self._compute_statistics(self.workers)

    def get_project_statistics(self, project_id):
        workers = self.get_workers_by_project_id(project_id)
        return self._compute_statistics(workers, project_id)
