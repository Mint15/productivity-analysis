def startswith(value, prefix):
    if not isinstance(value, list):
        return []
    return [item for item in value if item.startswith(prefix)]
