document.addEventListener('DOMContentLoaded', function () {
    const performanceData = JSON.parse(document.getElementById('performanceData').value);
    const bugReportData = JSON.parse(document.getElementById('bugReportData').value);
    const thresholds = JSON.parse(document.getElementById('settingsData').value).thresholds;

    if (performanceData.total_issues === 0) {
        document.getElementById('averageDelay').textContent = 'Нет данных';
        document.getElementById('overdueChart').style.display = 'none';
        document.getElementById('overdueTasksList').style.display = 'none';
        return;
    }

    const averageDelayHours = Math.floor(performanceData.average_difference);
    const averageDelayMinutes = Math.round((performanceData.average_difference - averageDelayHours) * 60);
    document.getElementById('averageDelay').textContent = `Средняя задержка: ${averageDelayHours} ч ${averageDelayMinutes} мин`;

    const overdueRatio = performanceData.overdue_ratio;
    const onTimeRatio = 1 - overdueRatio;

    var ctx = document.getElementById('overdueChart').getContext('2d');
    var overdueChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['Просрочено', 'В срок'],
            datasets: [{
                label: 'Производительность по задачам',
                data: [overdueRatio, onTimeRatio],
                backgroundColor: ['#ff6384', '#4bc0c0']
            }]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
            }
        }
    });

    if (overdueRatio > thresholds.overdue_task_ratio) {
        displayWarning('Доля просроченных задач превышает допустимый порог!', 'trackerActivityContent');
    }

    const tasksList = document.getElementById('overdueTasksList');
    if (performanceData.overdue_tasks.length > 0) {
        performanceData.overdue_tasks.forEach(task => {
            const taskItem = document.createElement('li');
            taskItem.classList.add("task-item");
            taskItem.innerHTML = `
                <span class="task-time"><i class="fas fa-clock m-1"></i> ${Math.floor(task.overdue_value)} ч ${Math.round((task.overdue_value - Math.floor(task.overdue_value)) * 60)} мин</span>
                <a href="${task.url}" target="_blank" class="task-summary">${task.summary}</a>
                <a href="${task.url}" target="_blank" class="btn btn-sec"><i class="fas fa-arrow-right"></i></a>`;
            tasksList.appendChild(taskItem);
        });
        document.getElementById('overdueTasksSection').style.display = 'flex';
    } else {
        document.getElementById('overdueTasksSection').style.display = 'none';
    }

    const wordsList = document.getElementById('frequentWordsList');
    if (bugReportData.frequent_words_details.length > 0) {
        bugReportData.frequent_words_details.sort((a, b) => b.count - a.count);

        bugReportData.frequent_words_details.forEach(wordDetail => {
            const wordItem = document.createElement('div');
            wordItem.classList.add('word-info');
            wordItem.innerHTML = `<div class="section-header" onclick="toggleCommentsVisibility(this)">
                                    <strong>(${wordDetail.count}) ${wordDetail.display}</strong>
                                  <span class="toggle-arrow right"></span></div>                              
                                  <div id="comments-${wordDetail.word}" class="section-content mt-1"></div>
                                 `;
            wordsList.appendChild(wordItem);
            const commentsList = document.getElementById(`comments-${wordDetail.word}`);
            wordDetail.comments.forEach(comment => {
                const commentItem = document.createElement('li');
                commentItem.classList.add("word-item")
                commentItem.innerHTML = `<a href="${comment.issue_url}" target="_blank" class="btn btn-link">${comment.issue_name}</a> <p>${comment.text}</p>`;
                commentsList.appendChild(commentItem);
            });
        });

        document.querySelectorAll('.section-content').forEach(section => {
            section.style.display = 'none';
        });
        document.getElementById('frequentWordsSection').style.display = 'flex';
    } else {
        document.getElementById('frequentWordsSection').style.display = 'none';
    }
});

function toggleSection(sectionId, headerElement) {
    const section = document.getElementById(sectionId);
    const arrow = headerElement.querySelector('.toggle-arrow');
    if (section.style.display === 'none' || section.style.display === '') {
        section.style.display = 'block';
        arrow.classList.remove('right');
        arrow.classList.add('down');
    } else {
        section.style.display = 'none';
        arrow.classList.remove('down');
        arrow.classList.add('right');
    }
}

function toggleCommentsVisibility(sectionHeader) {
    const commentsList = sectionHeader.nextElementSibling;
    const arrowElement = sectionHeader.querySelector('.toggle-arrow');
    if (commentsList.style.display === 'none' || commentsList.style.display === '') {
        commentsList.style.display = 'block';
        arrowElement.classList.remove('right');
        arrowElement.classList.add('down');
    } else {
        commentsList.style.display = 'none';
        arrowElement.classList.remove('down');
        arrowElement.classList.add('right');
    }
}
