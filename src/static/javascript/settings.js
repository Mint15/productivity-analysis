document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('settingsForm');
    const initialSettings = {};

    const settings = JSON.parse(document.getElementById('settingsData').value).thresholds;
    const defaultSettings = JSON.parse(document.getElementById('defaultSettingsData').value).thresholds;

    document.querySelectorAll('input[type=range], input[type=number]').forEach(input => {
        initialSettings[input.id] = input.value;
        setupInputBindings(input);
        updatePairedInput(input);
    });

    setSettings(settings);

    window.resetForm = function () {
        setSettings(settings)
    };

    window.setDefault = function () {
        setSettings(defaultSettings);
    };
});

function setupInputBindings(input) {
    input.oninput = function () {
        updatePairedInput(input);
    };
}

function updatePairedInput(input) {
    const isWarning = input.id.includes('_warning');
    const isCritical = input.id.includes('_critical');
    const isNumber = input.id.includes('_number');
    const isSingle = !isWarning && !isCritical;

    let pairedInputId;
    if (isSingle) {
        pairedInputId = input.id.includes('_number')
            ? input.id.replace('_number', '')
            : input.id + '_number';
    } else {
        pairedInputId = input.id.includes('_number')
            ? input.id.replace('_number', '')
            : input.id + '_number';
    }

    const pairedInput = document.getElementById(pairedInputId);

    if (pairedInput) {
        pairedInput.value = input.value;
    }

    if (isWarning) {
        const criticalInputId = input.id.replace('_warning', '_critical');
        const criticalInput = document.getElementById(criticalInputId);
        if (criticalInput && parseFloat(criticalInput.value) < parseFloat(input.value)) {
            criticalInput.value = input.value;
            const criticalPairedInputId = criticalInput.id.includes('_number')
                ? criticalInput.id.replace('_number', '')
                : criticalInput.id + '_number';
            const criticalPairedInput = document.getElementById(criticalPairedInputId);
            if (criticalPairedInput) {
                criticalPairedInput.value = input.value;
            }
        }
    } else if (isCritical) {
        const warningInputId = input.id.replace('_critical', '_warning');
        const warningInput = document.getElementById(warningInputId);
        if (warningInput && parseFloat(warningInput.value) > parseFloat(input.value)) {
            warningInput.value = input.value;
            const warningPairedInputId = warningInput.id.includes('_number')
                ? warningInput.id.replace('_number', '')
                : warningInput.id + '_number';
            const warningPairedInput = document.getElementById(warningPairedInputId);
            if (warningPairedInput) {
                warningPairedInput.value = input.value;
            }
        }
    }
}

function setSettings(data) {
    for (let key in data) {
        const warningNumberInputId = key + '_warning_number';
        const warningSliderId = key + '_warning';
        const criticalNumberInputId = key + '_critical_number';
        const criticalSliderId = key + '_critical';

        const warningNumberInput = document.getElementById(warningNumberInputId);
        const warningSlider = document.getElementById(warningSliderId);
        const criticalNumberInput = document.getElementById(criticalNumberInputId);
        const criticalSlider = document.getElementById(criticalSliderId);
        const singleNumberInput = document.getElementById(key + '_number');
        const singleSlider = document.getElementById(key);

        if (singleSlider && singleNumberInput) {
            singleNumberInput.value = data[key];
            singleSlider.value = data[key];
        } else {
            if (warningNumberInput && warningSlider) {
                warningNumberInput.value = data[key + '_warning'];
                warningSlider.value = data[key + '_warning'];
            }
            if (criticalNumberInput && criticalSlider) {
                criticalNumberInput.value = data[key + '_critical'];
                criticalSlider.value = data[key + '_critical'];
            }
        }
    }
}
