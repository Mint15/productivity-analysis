document.addEventListener('DOMContentLoaded', function () {
    const detailedStatistics = JSON.parse(document.getElementById('detailedStatistics').value);
    const detailedCounts = detailedStatistics.detailed_counts;
    const averageCommunicationData = detailedStatistics.average_communication_data;
    const averagePerformanceData = detailedStatistics.average_performance_data;

    document.getElementById('averageOverdueRatio').innerText = `${(averagePerformanceData.overdue_ratio * 100).toFixed(2)}%`;
    document.getElementById('averageOverdueTime').innerText = formatTime(averagePerformanceData.average_difference * 3600);
    document.getElementById('averageResponseTime').innerText = formatTime(averageCommunicationData.average_answer_time * 3600);
    document.getElementById('averageNegativeFeedback').innerText = `${(averageCommunicationData.negative_feedback_ratio * 100).toFixed(2)}%`;
    document.getElementById('averageBugsPerTask').innerText = `${averagePerformanceData.average_bugs.toFixed(2)}`;

    const createStackedChart = (ctx, data, title) => {
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [''],
                datasets: [
                    {
                        label: 'Нет проблем',
                        data: [data.good],
                        backgroundColor: '#4bc0c0'
                    },
                    {
                        label: 'Рекомендации',
                        data: [data.warning],
                        backgroundColor: '#ffcd56'
                    },
                    {
                        label: 'Проблемы',
                        data: [data.critical],
                        backgroundColor: '#ff6384'
                    }
                ]
            },
            options: {
                indexAxis: 'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    x: {
                        beginAtZero: true,
                        stacked: true,
                        ticks: {
                            precision: 0 // Устанавливает отображение целых чисел
                        }
                    },
                    y: {
                        stacked: true
                    }
                },
                plugins: {
                    legend: {
                        display: true,
                        position: 'top'
                    },
                    title: {
                        display: true,
                        text: title
                    }
                }
            }
        });
    };

    function formatTime(seconds) {
        var hours = Math.floor(seconds / 3600);
        seconds %= 3600;
        var minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;

        function russianPlural(number, one, two, five) {
            if ((number % 100 < 10 || number % 100 >= 20) && (number % 10 == 1)) {
                return number + " " + one;
            } else if ((number % 100 < 10 || number % 100 >= 20) && (number % 10 >= 2 && number % 10 <= 4)) {
                return number + " " + two;
            } else {
                return number + " " + five;
            }
        }

        var responseArray = [];
        if (hours > 0) responseArray.push(russianPlural(hours, "час", "часа", "часов"));
        if (minutes > 0) responseArray.push(russianPlural(minutes, "минута", "минуты", "минут"));
        if (seconds >= 0) responseArray.push(russianPlural(seconds, "секунда", "секунды", "секунд"));

        return responseArray.join(" ");
    }

    const overdueTasksCtx = document.getElementById('overdueTasksChart').getContext('2d');
    createStackedChart(overdueTasksCtx, detailedCounts.overdue_tasks, 'Выполнение задач в срок по всем работникам');

    const responseTimeCtx = document.getElementById('responseTimeChart').getContext('2d');
    createStackedChart(responseTimeCtx, detailedCounts.response_time, 'Оперативность ответа в чатах по всем работникам');

    const negativeFeedbackCtx = document.getElementById('negativeFeedbackChart').getContext('2d');
    createStackedChart(negativeFeedbackCtx, detailedCounts.negative_feedback, 'Позитивное общение в чатах по всем работникам');

    const bugsPerTaskCtx = document.getElementById('bugsPerTaskChart').getContext('2d');
    createStackedChart(bugsPerTaskCtx, detailedCounts.bugs_per_task, 'Отсутствие багов по всем работникам');
});
