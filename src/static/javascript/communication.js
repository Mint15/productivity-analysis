document.addEventListener('DOMContentLoaded', function () {
    const data = JSON.parse(document.getElementById('communicationData').value);
    console.log(data);
    const thresholds = JSON.parse(document.getElementById('settingsData').value).thresholds;

    if (!data || Object.keys(data).length === 0) {
        const noDataText = document.getElementById('messageCount');
        noDataText.innerText = 'Нет данных';
    } else {
        var messageCount = data.messages_count ? data.messages_count : 0;
        document.getElementById('messageCount').innerText = 'Количество сообщений: ' + messageCount;

        var averageSeconds = data.average_answer_time;
        if (averageSeconds) {
            var responseText = formatTime(averageSeconds);
            document.getElementById('averageResponseTime').innerText = 'Среднее время ответа: ' + responseText;
        }

        if (data.analytics && Object.keys(data.analytics).length !== 0) {
            var negativeFeedback = data.analytics.negative || 0;
            var totalFeedback = negativeFeedback + (data.analytics.neutral || 0) + (data.analytics.positive || 0);
            var negativeRatio = totalFeedback ? negativeFeedback / totalFeedback : 0;

            var ctx = document.getElementById('sentimentChart').getContext('2d');
            var sentimentChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ['Негативные', 'Нейтральные', 'Позитивные'],
                    datasets: [{
                        label: 'Sentiment Analysis',
                        data: [negativeFeedback, data.analytics.neutral || 0, data.analytics.positive || 0],
                        backgroundColor: ['#ff6384', '#ffcd56', '#4bc0c0']
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                    }
                }
            });
        } else {
            document.getElementById('sentimentChart').innerHTML = 'Нет данных для аналитики';
        }
    }

    function formatTime(seconds) {
        var hours = Math.floor(seconds / 3600);
        seconds %= 3600;
        var minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;

        function russianPlural(number, one, two, five) {
            if ((number % 100 < 10 || number % 100 >= 20) && (number % 10 == 1)) {
                return number + " " + one;
            } else if ((number % 100 < 10 || number % 100 >= 20) && (number % 10 >= 2 && number % 10 <= 4)) {
                return number + " " + two;
            } else {
                return number + " " + five;
            }
        }

        var responseArray = [];
        if (hours > 0) responseArray.push(russianPlural(hours, "час", "часа", "часов"));
        if (minutes > 0) responseArray.push(russianPlural(minutes, "минута", "минуты", "минут"));
        if (seconds > 0) responseArray.push(russianPlural(seconds, "секунда", "секунды", "секунд"));

        return responseArray.join(" ");
    }
});
